Google Analytics Sync allows to synchronize information from Google Analytics to
Drupal saving information in node_counter table.
Information is synchronized in periodic intervals (via Drush or Cron) and
after being available in Drupal can be used for generic statistics, or for
creating views representing most popular items.

The module is specially useful for sites using a CDN or a Reverse Proxy in front
of Drupal that can not use the normal statistics module.

Usage
---------------
Configure correctly Google Analytics Reports to connect to GA API using your
key in admin/settings/google-analytics-reports .

After having access to the GA API, this module can synchronize data from GA
using two methods:
- Normal Drupal cron.
- Crontab calling a drush command (recommended).


Normal Drupal Cron
--------------------
Enable synchronization via Drupal Cron, in admin/settings/google-analytics-sync
Next time Drupal cron runs, node_counter table will be filled with data.

Drush command
-------------------
You can run daily synchronization using the drush command:
$ drush google-analytics-sync-day

Therefore, you can install a crontab job similar to the following:
*/20 * * * * cd /var/www/html &&  drush google-analytics-sync-day

That would synchronize/update daily information each 20 minutes.

Full synchronization
--------------------
The module supports synchronization of data present in GA from the past. You
can run full synchronization in /admin/settings/google-analytics-sync/full or
run the available drush command:
$ drush google-analytics-sync-full 2010-01-01
