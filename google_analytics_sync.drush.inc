<?php

/**
 * @file
 * drush integration for Google Analytics Sync.
 */


/**
 * implements hook_drush_command()
 */
function google_analytics_sync_drush_command() {
  $items = array();

  $items['google-analytics-sync-day'] = array(
    'description' => "Synchronize daily information from Google analytics",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL
  );

  $items['google-analytics-sync-full'] = array(
    'description' => "Synchronize full information from Google analytics",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments'   => array(
      'start-date'    => dt('Start date in format 2005-01-01'),
    ),
  );

  return $items;
}

function drush_google_analytics_sync_day() {
  google_analytics_day_sync();
}

function drush_google_analytics_sync_full($date = NULL) {
  $start = strtotime($date);
  $end = time();

  if (!isset($date)) {
    drush_print(t('You need to specify an argument date in the format 2005-01-01'));
    return;
  }

  $days_between = ceil(abs($end - $start) / 86400);

  // first operation
  $operations[] = array('google_analytics_sync_start', array());

  for ($i=-$days_between; $i<0; $i++) {
    $operations[] = array('google_analytics_sync_step', array($i, $days_between));
  }

  $batch = array(
    'operations' => $operations,
    'finished' => 'google_analytics_sync_end',
    'title' => t('Synchronized Google Analytics results'),
    'init_message' => t('Synchronization is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Synchronization process has encountered an error.')
  );

  batch_set($batch);
  $batch =& batch_get();
  $batch['progressive'] = FALSE;
  // Process the batch.
  drush_backend_batch_process();
}



